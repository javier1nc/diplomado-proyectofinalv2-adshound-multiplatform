﻿using System;
using System.Collections.Generic;
using AdsHound.Models;
using AdsHound.Services;              


using Xamarin.Forms;

namespace AdsHound
{
    public partial class AdDetailsPage : ContentPage
    {
        private AdService _adService = new AdService();
        private Ad _ad;

        public AdDetailsPage(Ad ad)
        {
            if (ad == null)
                throw new ArgumentNullException(nameof(ad));

            _ad = ad;

            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            //BindingContext = await _adService.GetAd(_ad.Alias);

            BindingContext =  _ad;

            //await BindingContext = _ad;

            base.OnAppearing();
        }
    }
}
