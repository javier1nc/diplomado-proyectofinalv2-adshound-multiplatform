using System;
using System.IO;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AdsHound.Data;
using AdsHound.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AdsHound
{
    public partial class App : Application
    {

        static TodoItemDatabase database;



        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }


        //public App()
        //{
        //    Resources = new ResourceDictionary();
        //    Resources.Add("primarySilver", Color.FromHex("4A4A4A"));
        //    Resources.Add("primaryDarkGreen", Color.FromHex("6FA22E"));

        //    var nav = new MainPage();

        //    //var nav = new NavigationPage(new TodoListPage());
        //    //nav.BarBackgroundColor = (Color)App.Current.Resources["primarySilver"];
        //    //nav.BarTextColor = Color.Silver;
        //    //nav.BarTextColor = Color.Silver;

        //    MainPage = nav;
        //}

        public static TodoItemDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new TodoItemDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "AdCuponSQLite.db3"));
                }
                return database;
            }
        }

        public int ResumeAtTodoId { get; set; }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
