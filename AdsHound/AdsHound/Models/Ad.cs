﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AdsHound.Models
{
    


    public class Ad
    {
        //[JsonProperty("id")]
        public string id { get; set; }

        //[JsonProperty("alias")]
        public string alias { get; set; }

        //[JsonProperty("name")]
        public string name { get; set; }

        //[JsonProperty("image_url")]
        public string image_url { get; set; }


        //[JsonProperty("is_closed")]
        public Boolean is_closed { get; set; }

        //[JsonProperty("url")]
        public string url { get; set; }

        //[JsonProperty("review_count")]
        public string review_countd { get; set; }

        //[JsonProperty("categories")]
        public List<categories> categories { get; set; }

        //[JsonProperty("rating")]
        public string rating { get; set; }

        //[JsonProperty("phone")]
        public string phone { get; set; }


    }


    public class categories
    {
        public string alias { get; set; }

        public string title { get; set; }

    }

    //public class businesses<T>
    //{
    //    public List<T> Data { get; set; }

    //}
}

public class categories
{
    //[JsonProperty("alias")]
    public string alias { get; set; }

    //[JsonProperty("phone")]
    public string title { get; set; }
}
