﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AdsHound
{
    public partial class AdsHoundPage : ContentPage
    {
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new AdsHoundSearchPage());
        }

        public AdsHoundPage()
        {
            InitializeComponent();
        }
    }
}
