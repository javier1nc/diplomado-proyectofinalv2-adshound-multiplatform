﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdsHound.Models;

namespace AdsHound.Services
{
    public class SearchService
    {
        private List<Search> _searches = new List<Search>
        {
            new Search
            {
                Id = 1,
                Location = "Ciudad Universitaria, CDMX, Ciudad de Mexico",
                CheckIn = new DateTime(2018, 9, 1),
                CheckOut = new DateTime(2018, 11, 1)
            },
            new Search
            {
                Id = 2,
                Location = "San Ferando, CDMX, Ciudad de Mexico",
                CheckIn = new DateTime(2018, 9, 1),
                CheckOut = new DateTime(2018, 11, 1)
            },
            new Search
            {
                Id = 3,
                Location = "San , CDMX, Ciudad de Mexico",
                CheckIn = new DateTime(2018, 9, 1),
                CheckOut = new DateTime(2018, 11, 1)
            }
        };

        public IEnumerable<Search> GetRecentSearches(string filter = null)
        {
            if (String.IsNullOrWhiteSpace(filter))
                return _searches;

            // Note that I've used StringComparison.CurrentCultureIgnoreCase 
            // so searching is case-insensitive.
            return _searches.Where(s => s.Location.StartsWith(filter, StringComparison.CurrentCultureIgnoreCase));
        }

        public void DeleteSearch(int searchId)
        {
            _searches.Remove(_searches.Single(s => s.Id == searchId));
        }
    }
}
